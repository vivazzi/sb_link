=======
sb_link
=======

sb_link is django-cms plugin which is part of SBL (Service bussiness library, https://vits.pro/dev/).

This plugin adds link to page.


Installation
============

sb_link requires sb_core package: https://bitbucket.org/vivazzi/sb_core/

There is no sb_link in PyPI, so you can install this package from bitbucket repository only.

::
 
     $ pip install hg+https://bitbucket.org/vivazzi/sb_link


Configuration 
=============

1. Add "sb_link" to INSTALLED_APPS after "sb_core" ::

    INSTALLED_APPS = (
        ...
        'sb_core',
        'sb_link',
        ...
    )

2. Run `python manage.py migrate` to create the sb_link models.  

