from django.apps import AppConfig

from django.utils.translation import gettext_lazy as _


class SBLinkConfig(AppConfig):
    name = 'sb_link'
    verbose_name = _('Link')
