from django.utils.translation import gettext as _

from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase

from sb_core.mixins import RenderTemplateMixin
from sb_core.constants import BASE
from sb_link.forms import SBLinkForm
from sb_link.models import SBLink


class SBLinkPlugin(RenderTemplateMixin, CMSPluginBase):
    module = BASE
    model = SBLink
    name = _('Link / Button')
    render_template = 'sb_link/link.html'
    text_enabled = True
    form = SBLinkForm
    allow_children = True

    def get_fieldsets(self, request, obj=None):
        return (
            ('', {
                'fields': ('title',)
            }),
            (_('Type of Link'), {
                'fields': ('page_link', 'link', ('mailto', 'phone'))
            }),
            (_('Extra parameters'), {
                'fields': (('template', 'to_new_tab') if obj and obj.parent and obj.parent.plugin_type == 'TextPlugin'
                           else ('alignment', 'template', 'to_new_tab'),
                           'hint', 'anchor', 'rel', 'add_get_params', 'is_underlined')
            }),
        )

    def render(self, context, instance, placeholder):
        context = super(SBLinkPlugin, self).render(context, instance, placeholder)
        context['instance'].link_attrs = instance.get_link_attrs(context['request'])
        return context


plugin_pool.register_plugin(SBLinkPlugin)
