from cms.forms.utils import get_page_choices
from django import forms

from sb_core.mixins import TemplateFormMixin
from sb_link.models import SBLink


class SBLinkForm(TemplateFormMixin, forms.ModelForm):
    class Meta:
        exclude = ()
        model = SBLink

    def __init__(self, *args, **kwargs):
        super(SBLinkForm, self).__init__(*args, **kwargs)
        self.fields['page_link'].choices = get_page_choices()
