��          �   %   �      p  �   q     +  	   J  (   T     }     �  X   �     �     �     �  �        �     �     �     �     �     �     �  (   �       
        #     )     /     8     >     K  �  _  @    A   V     �  \   �  
   	     	  �   &	     �	     �	  /   �	  �   
     �
  B   �
  
   )     4     A  "   ]     �  K   �  =   �          1     @     M     Z     k  &                                  	                                                                       
                     A link to another site or page that is not contained in the CMS.<br/>If the domain name of the link matches this site, the link will take a relative form, for example: "/company/about/" Add string parameters to link? Alignment Allow search engines to follow the link? Anchor Button By default internal link opens in current window, but external links opens in new window Center Email Extra parameters Go to a specific place on the page.<br/>For details on the page: <a href="http://vits.pro/info/components/link/">What is an anchor?</a> Hint Internal link to the site page Left Link Link / Button Link to page Links Multiple link types cannot be specified. Open link in new tab? Other link Phone Right Template Title Type of Link Underline the link? Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-12-20 12:11+0800
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Ссылка на другой сайт или страницу, которая не содержится в CMS.<br>Если доменное имя ссылки будет совпадать с этим сайтом, то ссылка примет относительный вид, например: "/company/about/" Добавить параметры строки к ссылке? Выравнивание Разрешить поисковым роботам переходить по ссылке? Якорь Кнопка По умолчанию внутренние ссылки открываются в текущем окне, а внешние - в новом окне По центру Email Дополнительные параметры Переход в определённое место на странице.<br/>Подробнее на странице: <a href="http://vits.pro/info/components/link/">Что такое якорь?</a> Подсказка Внутренняя ссылка на страницу сайта Влево Ссылка Ссылка / Кнопка Ссылка на страницу Ссылки Нельзя указывать несколько типов ссылки. Открывать ссылку в новой вкладке? Другая ссылка Телефон Вправо Шаблон Название Тип ссылки Подчеркивать ссылку? 