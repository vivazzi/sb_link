from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_link', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sblink',
            name='is_in_text',
        ),
        migrations.AddField(
            model_name='sblink',
            name='mailto',
            field=models.EmailField(max_length=75, null=True, verbose_name='Email', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sblink',
            name='phone',
            field=models.CharField(max_length=40, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
            preserve_default=True,
        ),
    ]
