from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_link', '0002_auto_20150330_1128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sblink',
            name='mailto',
            field=models.EmailField(max_length=254, null=True, verbose_name='Email', blank=True),
        ),
    ]
