from django.db import models, migrations


def set_blank(apps, schema_editor):
    fields = ('anchor', 'hint', 'link', 'template', 'title')
    for obj in apps.get_model('sb_link', 'SBLink').objects.all():
        old_field_values = {field: getattr(obj, field) for field in fields}

        for field in fields:
            setattr(obj, field, getattr(obj, field) or '')

        dirty = False
        for field, value in old_field_values.items():
            if value != getattr(obj, field):
                dirty = True
        if dirty:
            obj.save()


class Migration(migrations.Migration):

    dependencies = [
        ('sb_link', '0006_auto_20160601_1833'),
    ]

    operations = [
        migrations.RunPython(set_blank),
        migrations.AlterField(
            model_name='sblink',
            name='anchor',
            field=models.CharField(default='', help_text='\u041f\u0435\u0440\u0435\u0445\u043e\u0434 \u0432 \u043e\u043f\u0440\u0435\u0434\u0435\u043b\u0451\u043d\u043d\u043e\u0435 \u043c\u0435\u0441\u0442\u043e \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435.<br/>\u041f\u043e\u0434\u0440\u043e\u0431\u043d\u0435\u0435 \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435: <a rel="nofollow" href="http://vits.pro/info/anchor">\u0427\u0442\u043e \u0442\u0430\u043a\u043e\u0435 \u044f\u043a\u043e\u0440\u044c?</a>', max_length=255, verbose_name='\u042f\u043a\u043e\u0440\u044c', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sblink',
            name='hint',
            field=models.CharField(default='', help_text='\u041f\u043e\u044f\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u043f\u0440\u0438 \u043d\u0430\u0432\u0435\u0434\u0435\u043d\u0438\u0438 \u043c\u044b\u0448\u043a\u043e\u0439 \u043d\u0430 \u043e\u0431\u044a\u0435\u043a\u0442', max_length=255, verbose_name='\u041f\u043e\u0434\u0441\u043a\u0430\u0437\u043a\u0430', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sblink',
            name='link',
            field=models.CharField(default='', help_text='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0434\u0440\u0443\u0433\u043e\u0439 \u0441\u0430\u0439\u0442 \u0438\u043b\u0438 \u0441\u0442\u043e\u0440\u043e\u043d\u043d\u0438\u0439 \u0440\u0435\u0441\u0443\u0440\u0441.', max_length=255, verbose_name='\u0412\u043d\u0435\u0448\u043d\u044f\u044f \u0441\u0441\u044b\u043b\u043a\u0430', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sblink',
            name='template',
            field=models.CharField(default='', max_length=255, verbose_name='\u0428\u0430\u0431\u043b\u043e\u043d \u043a\u043d\u043e\u043f\u043a\u0438', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sblink',
            name='title',
            field=models.CharField(default='', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=False,
        ),
    ]
