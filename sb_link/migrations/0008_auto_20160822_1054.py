from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_link', '0007_auto_20160809_1326'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sblink',
            name='anchor',
            field=models.CharField(help_text='\u041f\u0435\u0440\u0435\u0445\u043e\u0434 \u0432 \u043e\u043f\u0440\u0435\u0434\u0435\u043b\u0451\u043d\u043d\u043e\u0435 \u043c\u0435\u0441\u0442\u043e \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435.<br/>\u041f\u043e\u0434\u0440\u043e\u0431\u043d\u0435\u0435 \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435: <a href="http://vits.pro/info/components/link/">\u0427\u0442\u043e \u0442\u0430\u043a\u043e\u0435 \u044f\u043a\u043e\u0440\u044c?</a>', max_length=255, verbose_name='\u042f\u043a\u043e\u0440\u044c', blank=True),
        ),
    ]
