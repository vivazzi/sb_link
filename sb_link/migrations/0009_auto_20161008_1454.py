from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_link', '0008_auto_20160822_1054'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sblink',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='sb_link_sblink', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE),
        ),
    ]
