# Generated by Django 1.9.11 on 2017-02-27 16:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sb_link', '0011_sblink_add_get_params'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sblink',
            name='link',
            field=models.CharField(blank=True, help_text='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443, \u043a\u043e\u0442\u043e\u0440\u0430\u044f \u043d\u0435 \u0441\u043e\u0434\u0435\u0440\u0436\u0438\u0442\u0441\u044f \u0432 CMS, \u0434\u0440\u0443\u0433\u043e\u0439 \u0441\u0430\u0439\u0442 \u0438\u043b\u0438 \u0441\u0442\u043e\u0440\u043e\u043d\u043d\u0438\u0439 \u0440\u0435\u0441\u0443\u0440\u0441.<br>\u0415\u0441\u043b\u0438 \u0434\u043e\u043c\u0435\u043d\u043d\u043e\u0435 \u0438\u043c\u044f \u0441\u0441\u044b\u043b\u043a\u0438 \u0431\u0443\u0434\u0435\u0442 \u0441\u043e\u0432\u043f\u0430\u0434\u0430\u0442\u044c \u0441 \u044d\u0442\u0438\u043c \u0441\u0430\u0439\u0442\u043e\u043c, \u0442\u043e \u0441\u0441\u044b\u043b\u043a\u0430 \u043f\u0440\u0438\u043c\u0435\u0442 \u043e\u0442\u043d\u043e\u0441\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u0432\u0438\u0434, \u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: "/company/about/" ', max_length=255, verbose_name='\u0414\u0440\u0443\u0433\u0430\u044f \u0441\u0441\u044b\u043b\u043a\u0430'),
        ),
    ]
