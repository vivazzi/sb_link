# Generated by Django 3.1.7 on 2021-06-04 07:36

from django.db import migrations, models
import sb_core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sb_link', '0021_auto_20210601_0102'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sblink',
            name='is_underlined',
            field=models.BooleanField(blank=True, choices=[(None, 'По умолчанию'), (True, 'Да'), (False, 'Нет')], null=True, verbose_name='Underline the link?'),
        ),
        migrations.AlterField(
            model_name='sblink',
            name='rel',
            field=models.BooleanField(blank=True, choices=[(None, 'По умолчанию'), (True, 'Да'), (False, 'Нет')], help_text='По умолчанию переход поисковыми роботами по внешним ссылкам запрещён, а для внутренних - разрешён.<br/>Это позволяет не снижать позиции сайта в поисковых системах.<br/>Подробнее на странице: <a rel="nofollow" target="_blank" href="http://vits.pro/info/site-positions/">Позиции сайта в поисковых системах</a>', null=True, verbose_name='Allow search engines to follow the link?'),
        ),
        migrations.AlterField(
            model_name='sblink',
            name='template',
            field=sb_core.fields.TemplateField(blank=True, max_length=255, verbose_name='Template'),
        ),
        migrations.AlterField(
            model_name='sblink',
            name='to_new_tab',
            field=models.BooleanField(blank=True, choices=[(None, 'По умолчанию'), (True, 'Да'), (False, 'Нет')], help_text='By default internal link opens in current window, but external links opens in new window', null=True, verbose_name='Open link in new tab?'),
        ),
    ]
