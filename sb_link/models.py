from urllib.parse import urlsplit

from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ValidationError
from django.db import models
from cms.models import CMSPlugin, Page
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from sb_core.constants import hint_ht, DIGITS, rel_ht, NULL_BOOLEAN_DEFAULT_CHOICES
from sb_core.fields import TemplateField
from sb_core.template_pool import template_pool


class SBLink(CMSPlugin):
    title = models.CharField(_('Title'), max_length=255, blank=True)
    hint = models.CharField(_('Hint'), max_length=255, blank=True, help_text=hint_ht)

    page_link = models.ForeignKey(Page, verbose_name=_('Link to page'), max_length=255, null=True, blank=True,
                                  help_text=_('Internal link to the site page'), on_delete=models.CASCADE)

    link = models.CharField(
        _('Other link'), max_length=255, blank=True,
        help_text=_('A link to another site or page that is not contained in the CMS.<br/>'
                    'If the domain name of the link matches this site, the link will take a relative form, '
                    'for example: "/company/about/"'))

    mailto = models.EmailField(_('Email'), blank=True)
    phone = models.CharField(_('Phone'), blank=True, max_length=40)

    anchor = models.CharField(
        _('Anchor'), max_length=255, blank=True,
        help_text=_('Go to a specific place on the page.<br/>For details on the page: '
                    '<a href="http://vits.pro/info/components/link/">What is an anchor?</a>'))

    to_new_tab = models.BooleanField(_('Open link in new tab?'), choices=NULL_BOOLEAN_DEFAULT_CHOICES, null=True, blank=True,
                                     help_text=_('By default internal link opens in current window, '
                                                 'but external links opens in new window'))

    rel = models.BooleanField(_('Allow search engines to follow the link?'), choices=NULL_BOOLEAN_DEFAULT_CHOICES,
                              help_text=rel_ht, null=True, blank=True)
    LEFT = 'left'
    CENTER = 'center'
    RIGHT = 'right'
    BLOCK_CHOICES = ((LEFT, _('Left')),
                     (CENTER, _('Center')),
                     (RIGHT, _('Right')))

    alignment = models.CharField(_('Alignment'), max_length=10, blank=True, choices=BLOCK_CHOICES)
    is_underlined = models.BooleanField(_('Underline the link?'), choices=NULL_BOOLEAN_DEFAULT_CHOICES, null=True, blank=True)

    add_get_params = models.BooleanField(_('Add string parameters to link?'), default=False)

    template = TemplateField(_('Template'), blank=True)

    def get_link(self, request):
        if self.phone:
            link = 'tel:{}'.format(self.phone)
        elif self.mailto:
            link = 'mailto:{}'.format(self.mailto)
        elif self.link:
            link = self.link
        elif self.page_link_id:
            link = self.page_link.get_absolute_url()
        else:
            link = ''

        link = '{}#{}'.format(link, self.anchor) if self.anchor else link
        link = '{}?{}'.format(link, request.GET.urlencode()) if self.add_get_params and request.GET.urlencode else link

        return link

    def get_link_attrs(self, request):

        if self.rel is False or self.rel == '' and self.link.startswith('http'): rel = 'rel="nofollow"'
        else: rel = ''

        link = 'href="{}"'.format(self.get_link(request)) if self.get_link(request) else ''
        hint = 'title="{}"'.format(escape(self.hint)) if self.hint else ''
        target = 'target="_blank"' if self.to_new_tab or self.to_new_tab == '' and self.link.startswith('http') else ''

        return mark_safe(' '.join([rel, link, hint, target]))

    def get_title(self):
        if self.title:
            return self.title

        if self.phone:
            return self.phone
        elif self.mailto:
            return self.mailto
        elif self.link:
            return self.link[2:] if self.link.startswith('//') else self.link
        elif self.page_link_id:
            return self.page_link.get_title()

        return ''

    def __str__(self):
        title = self.get_title()
        if self.template:
            return f'{title}{template_pool.obj_template_str(self, use_brackets=True)}'

        return title

    def save(self, no_signals=False, *args, **kwargs):
        phone = ''
        if self.phone:
            for w in self.phone:
                if w in DIGITS: phone += w

        if not self.title:
            self.title = self.phone

        self.phone = phone

        if self.anchor:
            self.anchor = self.anchor.replace('#', '')

        if self.link and self.link[0] != '/':
            if not self.link.startswith(('http', '#', '?')):
                self.link = '//{}'.format(self.link)

            url = urlsplit(self.link)
            site = get_current_site(None)
            if url.netloc == site.name:
                self.link = url.path
                if url.query:
                    self.link += '?{}'.format(url.query)
                if url.fragment:
                    self.link += '#{}'.format(url.fragment)

        super(SBLink, self).save(*args, **kwargs)

    def clean(self):
        links = [self.link, self.page_link, self.mailto, self.phone]
        count = sum([1 for link in links if link])
        if count > 1:
            raise ValidationError(_('Multiple link types cannot be specified.'))

    class Meta:
        db_table = 'sb_link'
        verbose_name = _('Link')
        verbose_name_plural = _('Links')
