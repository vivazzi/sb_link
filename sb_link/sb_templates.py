from django.utils.translation import gettext_lazy as _

from sb_core.template_pool import template_pool, Template
from sb_link.models import SBLink


@template_pool.register(model=SBLink)
class Button(Template):
    template = 'sb_link/btn.html'
    title = _('Button')
